import java.util.Arrays;

public class RemoveDuplicates {
    public static int RemoveDuplicates(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }

        int uniqueCount = 1;
        int current = nums[0];

        for (int i = 1; i < nums.length; i++) {
            if (nums[i] != current) {
                current = nums[i];
                nums[uniqueCount] = current;
                uniqueCount++;
            } else {
                nums[i] = '_';
            }
        }

        return uniqueCount;
    }

    public static void main(String[] args) {
        int[] nums = { 0, 0, 1, 1, 1, 2, 2, 3, 3, 4 };

        System.out.println("Input: nums = " + Arrays.toString(nums));

        int uniqueElementsCount = RemoveDuplicates(nums);
        System.out.print("Output: " + uniqueElementsCount + ", nums = [");
        for (int i = 0; i < nums.length; i++) {
            if (i < uniqueElementsCount) {
                System.out.print(nums[i]);
            } else {
                System.out.print("_");
            }

            if (i < nums.length - 1) {
                System.out.print(", ");
            }
        }
        System.out.println("]");
    }
}
