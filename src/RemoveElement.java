public class RemoveElement {
    public static int removeElement(int[] nums, int val) {
        int k = 0; 
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                nums[k] = nums[i];
                k++;
            }
        }

        return k;
    }

    public static void main(String[] args) {
        int[] nums = { 3, 2, 2, 3 };
        int val = 3;

        System.out.println("Input: nums = " + getFormattedArray(nums) + ", val = " + val);

        int k = removeElement(nums, val);
        System.out.println("Output: " + k + ", nums = " + getFormattedArray(nums, k));
    }

    public static String getFormattedArray(int[] nums) {
        StringBuilder sb = new StringBuilder();

        sb.append("[");
        for (int i = 0; i < nums.length; i++) {
            sb.append(nums[i]);

            if (i < nums.length - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");

        return sb.toString();
    }

    public static String getFormattedArray(int[] nums, int k) {
        StringBuilder sb = new StringBuilder();

        sb.append("[");
        for (int i = 0; i < nums.length; i++) {
            if (i < k) {
                sb.append(nums[i]);
            } else {
                sb.append("_");
            }

            if (i < nums.length - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");

        return sb.toString();
    }
}
