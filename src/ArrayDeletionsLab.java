public class ArrayDeletionsLab {
    public static int[] deleteElementByIndex(int[] arr, int index) {
        if (index < 0 || index >= arr.length) {
            System.out.println("Invalid index!");
            return arr;
        }

        int[] updatedArray = new int[arr.length - 1];
        int j = 0;

        for (int i = 0; i < arr.length; i++) {
            if (i != index) {
                updatedArray[j] = arr[i];
                j++;
            }
        }

        return updatedArray;
    }

    public static int[] deleteElementByValue(int[] arr, int value) {
        int index = -1;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                index = i;
                break;
            }
        }

        if (index == -1) {
            System.out.println("Value not found in the array!");
            return arr;
        }

        return deleteElementByIndex(arr, index);
    }

    public static void main(String[] args) {
        int[] originalArray = { 1, 2, 3, 4, 5 };

        System.out.println("Original Array: " + getFormattedArray(originalArray));

        // Test deleteElementByIndex method
        int[] updatedArray1 = deleteElementByIndex(originalArray, 2);
        System.out.println("Array after deleting element at index 2: " + getFormattedArray(updatedArray1));

        // Test deleteElementByValue method
        int[] updatedArray2 = deleteElementByValue(originalArray, 4);
        System.out.println("Array after deleting element with value 4: " + getFormattedArray(updatedArray2));
    }

    public static String getFormattedArray(int[] arr) {
        StringBuilder sb = new StringBuilder();

        sb.append("[");
        for (int i = 0; i < arr.length; i++) {
            sb.append(arr[i]);

            if (i < arr.length - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");

        return sb.toString();
    }
}
